<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;

$this->title = 'Pogoda Polska na 5 dni';
?>


<div class="site-index">

    <div class="jumbotron">
        <h2>Pogoda Polska na 5 dni</h2>
    </div>

    <div class="body-content">
        <div>
            <div class="dropdown">
                <button class="btn btn-lg dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    wybierz miasto
                </button>
                <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                    <ul id="menu">
                        <?= $cityList; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <?= $content; ?>
        </div>
    </div>
</div>
