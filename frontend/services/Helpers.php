<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 28.09.17
 * Time: 15:47
 */

namespace frontend\services;


use Yii;
use yii\helpers\Url;

class Helpers
{
    public static function getCityList($patch, $cantry)
    {
        $tmp = null;
        foreach (self::getJsonList($patch, $cantry) as $item) {
            $url = Url::toRoute(['site/index', 'id' => $item['id']]);
            $tmp .= '<li><a class="dropdown-item" href="' . $url . '">' . $item['name'] . '</a> </li>';
        }
        return $tmp;
    }

    public static function generateContent($weather)
    {
        $list = $weather['list'];
        $tmp_date = 0;

        $tmp = '<h2>' . $weather['city']['name'] . '</h2>';
        $tmp .= '<table><tr>';
        $tmp .= '<td colspan="8" height="30" class="tdstrong">' . date("d-m-Y") . '</td></tr><tr>';

        foreach ($list as $item) {

            $date = date("d-m-Y", $item['dt']);
            $temp = self::round($item['main']['temp']);
            $temp_min = self::round($item['main']['temp_min']);
            $temp_max = self::round($item['main']['temp_max']);
            $desc = $item['weather'][0]['description'];

            if ($tmp_date != $date) {

                if ($tmp_date != 0) {

                    $tmp .= '</tr><tr>';
                    $tmp .= '<td colspan="8" height="30" class="tdstrong">' . $date . '</td></tr><tr>';
                }
                $tmp_date = $date;
            }
            $tmp .= '<td>' .
                '<strong>' . date("H:i", $item['dt']) . '</strong>'
                . '<br /> + '
                . $temp . '<br /> + '
                . $temp_min . '<br /> + '
                . $temp_max . '<br />'
                . $desc . '<br />'
                . '</td>';
        }

        $tmp .= '</tr><table>';
        return $tmp;
    }


    public static function downloadWeather($city_id)
    {
        $info = file_get_contents('http://api.openweathermap.org/data/2.5/forecast?id=' . $city_id . '&APPID=' . Yii::$app->params['key_api'] . '');
        $weather = (json_decode($info, true));

        return $weather;
    }

    private static function getJsonList($patch, $cantry)
    {
        $cities = file_get_contents($patch);
        $arr = json_decode($cities, true);
        $sities_pl = array();

        foreach ($arr as $item) {
            if ($item['country'] == $cantry) $sities_pl[] = $item;
        }
        return $sities_pl;
    }

    private static function round($num)
    {
        return ceil($num - 273.15);
    }
}