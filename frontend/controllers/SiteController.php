<?php

namespace frontend\controllers;

use frontend\services\Helpers;
use Yii;
use yii\web\Controller;


/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex($id = NULL)
    {
        $weather = $content = NULL;
        $cityList = Helpers::getCityList('city.json', 'PL');
        if ($id) {
            $weather = Helpers::downloadWeather($id);
            $content = Helpers::generateContent($weather);
        }
        return $this->render('index', compact('cityList', 'content', 'weather'));
    }
}
